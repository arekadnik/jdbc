package jdbcExercise.DAOimplementation;

import jdbcExercise.DAOimplementation.DAOs.CarDAO;
import jdbcExercise.DAOimplementation.DAOs.EmployeeDAO;
import jdbcExercise.DAOimplementation.DAOs.ProductDAO;
import jdbcExercise.DAOimplementation.entities.Car;
import jdbcExercise.DAOimplementation.entities.Employee;
import jdbcExercise.DAOimplementation.entities.Product;

import java.util.List;

public class Demo {
    public static void main(String[] args) {

//        Employee employee = new Employee(1111,"jasiu","kupa",23);
//        EmployeeDAO employeeDAO = new EmployeeDAO();
//        employeeDAO.delete(employee);

//        EmployeeDAO employeeDAO = new EmployeeDAO();
//        List<Employee> employeeArrayList = employeeDAO.select();
//        for (Employee employee : employeeArrayList) {
//            System.out.println(employee);
//        }

//        Product product = new Product(212,"Cola",1222,"nokia");
//        ProductDAO productDAO = new ProductDAO();
//        productDAO.insert(product);

        Car car = new Car(222,"volkswagen","kombi",2000,5);
        CarDAO carDAO = new CarDAO();
        carDAO.insert(car);

    }
}
