package jdbcExercise.DAOimplementation.sqlGenerators;

import jdbcExercise.DAOimplementation.entities.Car;

public class CarSqlGenerator implements SqlGenerator<Car> {
    @Override
    public String insert(Car toInsert) {
        StringBuilder sb = new StringBuilder();
        sb.append("Insert into Car ")
                .append("(id, model, typee, engineCapacity,howManyPeople) values(")
                .append(toInsert.getId())
                .append(", '").append(toInsert.getModel()).append("'")
                .append(", '").append(toInsert.getTypee()).append("'")
                .append(", ").append(toInsert.getEngineCapacity())
                .append(", ").append(toInsert.getHowManyPeople()).append(");");
        return sb.toString();
    }

    @Override
    public String selectAll() {
        return "Select * from Car;";
    }

    @Override
    public String update(Car toUpdate) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE Car SET model='")
                .append(toUpdate.getModel()).append("',")
                .append("typee='").append(toUpdate.getTypee()).append("',")
                .append("engineCapacity=").append(toUpdate.getEngineCapacity())
                .append(" where id =").append(toUpdate.getId()).append(";");
        return sb.toString();
    }

    @Override
    public String delete(Car toDelete) {
        return "delete from Car where id=" + toDelete.getId() + ";";
    }
}
