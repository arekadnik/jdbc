package jdbcExercise.DAOimplementation.sqlGenerators;

import jdbcExercise.DAOimplementation.entities.Employee;
import jdbcExercise.DAOimplementation.entities.Product;

import java.util.function.Predicate;

public class ProductSqlGenerator implements SqlGenerator<Product> {
    @Override
    public String insert(Product toInsert) {
        StringBuilder sb = new StringBuilder();
        sb.append("Insert into Product ")
                .append("(id, name, price, supplier) values(")
                .append(toInsert.getId())
                .append(", '").append(toInsert.getName()).append("'")
                .append(",").append(toInsert.getPrice()).append(", '")
                .append(toInsert.getSupplier()).append("');");
        return sb.toString();
    }

    @Override
    public String selectAll() {
        return "Select * from Product;";
    }

    @Override
    public String update(Product toUpdate) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE Product SET name='")
                .append(toUpdate.getName()).append("',")
                .append("price=").append(toUpdate.getPrice()).append(",")
                .append("supplier=").append(toUpdate.getSupplier())
                .append(" where id =").append(toUpdate.getId()).append(";");
        return sb.toString();
    }

    @Override
    public String delete(Product toDelete) {
        return "delete from Employees where id=" + toDelete.getId() + ";";
    }
}
