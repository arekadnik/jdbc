package jdbcExercise.DAOimplementation.entities;

public class Product implements Entity {
    private int id;
    private String name;
    private int price;
    private String supplier;


    public Product(){

    }

    public Product(int id, String name, int price, String supplier) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.supplier = supplier;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", supplier='" + supplier + '\'' +
                '}';
    }
}
