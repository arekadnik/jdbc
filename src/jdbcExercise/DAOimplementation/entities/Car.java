package jdbcExercise.DAOimplementation.entities;

public class Car implements Entity{



    private int id;
    private String model;
    private String typee;
    private int engineCapacity;
    private int howManyPeople;

    public Car(){

    }

    public Car(int id, String model, String type, int engineCapacity, int howManyPeople) {
        this.id = id;
        this.model = model;
        this.typee = type;
        this.engineCapacity = engineCapacity;
        this.howManyPeople = howManyPeople;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getTypee() {
        return typee;
    }

    public void setTypee(String typee) {
        this.typee = typee;
    }

    public int getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(int engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public int getHowManyPeople() {
        return howManyPeople;
    }

    public void setHowManyPeople(int howManyPeople) {
        this.howManyPeople = howManyPeople;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", model='" + model + '\'' +
                ", typee='" + typee + '\'' +
                ", engineCapacity=" + engineCapacity +
                ", howManyPeople=" + howManyPeople +
                '}';
    }
}
