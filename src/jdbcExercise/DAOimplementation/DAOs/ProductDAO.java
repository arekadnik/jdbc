package jdbcExercise.DAOimplementation.DAOs;

import jdbcExercise.DAOimplementation.BaseDAO;
import jdbcExercise.DAOimplementation.entities.Product;
import jdbcExercise.DAOimplementation.sqlGenerators.ProductSqlGenerator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductDAO extends BaseDAO<Product> {

    ProductSqlGenerator productSqlGenerator = new ProductSqlGenerator();

    @Override
    public void insert(Product toInsert) {

        sql = productSqlGenerator.insert(toInsert);
        execute();
    }

    @Override
    public List select() {
        sql = productSqlGenerator.selectAll();
        return executeSelect();
    }

    @Override
    public void update(Product toUpdate) {

        sql = productSqlGenerator.update(toUpdate);
        execute();
    }

    @Override
    public void delete(Product toDelete) {

        sql = productSqlGenerator.delete(toDelete);
        execute();
    }

    @Override

    protected List<Product> parse(ResultSet resultSet) {
        ArrayList<Product> result = new ArrayList<>();
        try {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String supplier = resultSet.getString("lastname");
                int price = resultSet.getInt("age");
                result.add(new Product(id, name, price, supplier));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}

