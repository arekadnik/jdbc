package jdbcExercise.DAOimplementation.DAOs;

import jdbcExercise.DAOimplementation.BaseDAO;
import jdbcExercise.DAOimplementation.entities.Car;
import jdbcExercise.DAOimplementation.sqlGenerators.CarSqlGenerator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CarDAO extends BaseDAO<Car> {
    CarSqlGenerator carSqlGenerator = new CarSqlGenerator();

    @Override
    public void insert(Car toInsert) {

        sql = carSqlGenerator.insert(toInsert);
        execute();
    }

    @Override
    public List select() {
        sql = carSqlGenerator.selectAll();
        return executeSelect();
    }

    @Override
    public void update(Car toUpdate) {
        sql = carSqlGenerator.update(toUpdate);
        execute();

    }

    @Override
    public void delete(Car toDelete) {
        sql = carSqlGenerator.delete(toDelete);
        execute();

    }

    @Override
    protected List<Car> parse(ResultSet resultSet) {
        ArrayList<Car> result = new ArrayList<>();
        try {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String model = resultSet.getString("model");
                String type = resultSet.getString("typee");
                int engineCapacity = resultSet.getInt("engineCapacity");
                int howManyPeople = resultSet.getInt("howManyPeople");
                result.add(new Car(id, model, type, engineCapacity, howManyPeople));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
